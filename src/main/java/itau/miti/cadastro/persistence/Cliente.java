package itau.miti.cadastro.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nome;
    @NotBlank
    private String cpfNumero;
    @NotBlank
    private String cpfFotoUrl;
    private LocalDate cadastradoEm;
    private Boolean cadastroEfetivado;
    private LocalDate efetivadoEm;
}

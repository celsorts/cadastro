package itau.miti.cadastro.application;

import itau.miti.cadastro.persistence.Cliente;
import lombok.Data;

@Data
public class DadosCadastrais {
    private String nome;
    private String cpfNumero;
    private String cpfFotoUrl;

    public Cliente getCliente(){
        Cliente cliente = new Cliente();
        cliente.setNome(nome);
        cliente.setCpfNumero(cpfNumero);
        cliente.setCpfFotoUrl(cpfFotoUrl);

        return cliente;
    }
}

package itau.miti.cadastro.application;

import itau.miti.cadastro.persistence.Cliente;
import itau.miti.cadastro.services.CadastroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Map;

@RestController
public class ClienteController {
    @Autowired
    private CadastroService cadastroService;

    @PostMapping
    public Cliente cadastrar(@RequestBody DadosCadastrais dados){
        Cliente cliente = dados.getCliente();
        return cadastroService.cadastrar(cliente);
    }

    @PatchMapping("/{numeroCpf}/efetivar")
    public Cliente efetivar(@PathVariable String numeroCpf){
        return cadastroService.efetivar(numeroCpf);
    }

    @GetMapping("/{numeroCpf}")
    public Cliente consultar(@PathVariable String numeroCpf){
        return cadastroService.buscarCliente(numeroCpf);
    }

    @ExceptionHandler(ClienteInexistenteException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void tratarClienteInexistente(){
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public Map<String, String> tratarErroValidacao(ConstraintViolationException exception){
        Map<String, String> erros = Map.of();

        for(ConstraintViolation<?> violation : exception.getConstraintViolations()){
            erros.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return erros;
    }
}

package itau.miti.cadastro.services;

import itau.miti.cadastro.persistence.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "operacao", url = "localhost:8080")
public interface CorrentistaClient {
    @PostMapping
    void abrirConta(@RequestBody Cliente cliente);
}

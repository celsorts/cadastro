package itau.miti.cadastro.services;

import itau.miti.cadastro.application.ClienteInexistenteException;
import itau.miti.cadastro.persistence.Cliente;
import itau.miti.cadastro.persistence.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class CadastroService {
    @Autowired
    private ClienteRepository repository;
    @Autowired
    private CorrentistaClient correntistaClient;

    public Cliente cadastrar(Cliente cliente){
        cliente.setCadastradoEm(LocalDate.now());
        cliente.setCadastroEfetivado(null);

        return repository.save(cliente);
    }

    public Cliente efetivar(String cpf){
        Cliente cliente = buscarCliente(cpf);

        correntistaClient.abrirConta(cliente);

        cliente.setCadastroEfetivado(true);
        cliente.setEfetivadoEm(LocalDate.now());

        return repository.save(cliente);
    }

    public Cliente buscarCliente(String cpf){
        Optional<Cliente> optional = repository.findByCpfNumero(cpf);

        if(optional.isEmpty()){
            throw new ClienteInexistenteException();
        }

        return optional.get();
    }
}
